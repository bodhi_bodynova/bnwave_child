<?php
/**
 *    This file is part of OXID eShop Community Edition.
 *
 *    OXID eShop Community Edition is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    OXID eShop Community Edition is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @package   lang
 * @copyright (C) OXID eSales AG 2003-2016
 * @version OXID eShop CE
 * @translators: alexraimondo (1), Alpha-Sys (5), chatard (318), cowboy9571 (10), ET (10), ftn2018 (236), marco (9), patmat2809 (5), Phenix (203), piccobello (1), Rainbow40 (2), rava3000 (8), vikapera (376)
 */

$sLangName = 'Français';

// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
    'charset' => 'utf-8',
    'BACK_TO_OVERVIEW' => 'Retour à l\'aperçu',
    'DD_BASKET_BACK_TO_SHOP' => 'Retour à la boutique en ligne',
    'DD_CLOSE_MODAL' => 'Fermer',
    'DD_CONTACT_ADDRESS_HEADING' => 'Adresse',
    'DD_CONTACT_FORM_HEADING' => 'Formulaire de contact',
    'DD_CONTACT_PAGE_HEADING' => 'Contactez-nous!',
    'DD_CONTACT_THANKYOU1' => 'Merci pour votre message à',
    'DD_CONTACT_THANKYOU2' => '.',
    'DD_CONTACT_SELECT_SALUTATION' => 'Veuillez sélectionner',
    'DD_DEMO_ADMIN_TOOL' => 'Ouvrir l\'interface d\'administration',
    'DD_DOWNLOADLINKS_HEADING' => 'Vos liens de téléchargement - commandes',
    'DD_DOWNLOADS_DOWNLOAD_TOOLTIP' => 'Télécharger',
    'DD_ERR_404_CONTACT_BUTTON' => 'vers le formulaire de contact',
    'DD_ERR_404_CONTACT_TEXT' => 'Besoin d\'aide ou des questions sur nos produits?<br>N\'hésitez pas à nous appeler ou à nous envoyer un e-mail:',
    'DD_ERR_404_START_BUTTON' => 'Aller à la page d\'accueil',
    'DD_ERR_404_START_TEXT' => 'Vous trouverez peut-être les informations souhaitées sur notre page d\'accueil:',
    'DD_FILE_ATTRIBUTES_FILENAME' => 'Nom de fichier:',
    'DD_FILE_ATTRIBUTES_FILESIZE' => 'Taille du fichier:',
    'DD_FILE_ATTRIBUTES_OCLOCK' => 'Heure',
    'DD_FOOTER_CONTACT_INFO' => 'Contact:',
    'DD_FOOTER_FOLLOW_US' => 'Suivez-nous:',
    'DD_FORGOT_PASSWORD_HEADING' => 'Mot de passe oublié',
    'DD_FORM_VALIDATION_CHECKONE' => 'Veuillez sélectionner au moins une option.',
    'DD_FORM_VALIDATION_INTEGER' => 'Les décimales ne sont pas autorisées.',
    'DD_FORM_VALIDATION_NEGATIVENUMBER' => 'Veuillez entrer un nombre négatif.',
    'DD_FORM_VALIDATION_NUMBER' => 'Veuillez entrer un nombre.',
    'DD_FORM_VALIDATION_PASSWORDAGAIN' => 'Les mots de passe ne correspondent pas.',
    'DD_FORM_VALIDATION_POSITIVENUMBER' => 'Veuillez entrer un nombre positif.',
    'DD_FORM_VALIDATION_REQUIRED' => 'Veuillez entrer une valeur pour ce champ obligatoire.',
    'DD_FORM_VALIDATION_VALIDEMAIL' => 'Veuillez entrer une adresse e-mail valide.',
    'DD_INVITE_HEADING' => 'Recommandation produit',
    'DD_INVITE_LINK' => 'Lien',
    'DD_LINKS_NO_ENTRIES' => 'Malheureusement, il n\'y a pas de liens disponibles.',
    'DD_LISTLOCATOR_FILTER_ATTRIBUTES' => 'Filtre:',
    'DD_LIST_SHOW_MORE' => 'Voir les produits...',
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_BODY' => 'En vous enregistrant dans notre shop, votre commande sera encore plus simple et rapide. Dans votre compte, vous pourrez enregistrer plusieurs adresses de livraison et suivre l\'état de vos commandes. ',
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_TITLE' => 'Créer un compte',
    'DD_LOGIN_ACCOUNT_PANEL_LOGIN_TITLE' => 'Connexion',
    'DD_MINIBASKET_CONTINUE_SHOPPING' => 'Continuer vos achats',
    'DD_MINIBASKET_MODAL_TABLE_PRICE' => 'Total',
    'DD_MINIBASKET_MODAL_TABLE_TITLE' => 'Article',
    'DD_NEWSLETTER_OPTIN_HEADING' => 'Votre inscription à la newsletter',
    'DD_ORDERSHIPPED_HEADING' => 'Confirmation d\'expédition\' - Commande',
    'DD_ORDER_CUST_HEADING' => 'Commande',
    'DD_ORDER_ORDERDATE' => 'Date:',
    'DD_OWNER_REMINDER_HEADING' => 'Petit stock',
    'DD_PRICEALARM_HEADING' => 'Alerte prix',
    'DD_PRODUCTMAIN_STOCKSTATUS' => 'Stock',
    'DD_RATING_CUSTOMERRATINGS' => 'Avis clients',
    'DD_RECOMMENDATION_EDIT_BACK_TO_LIST' => 'Retour à la vue d\'ensemble',
    'DD_REGISTER_HEADING' => 'Votre inscription',
    'DD_ROLES_BEMAIN_UIROOTHEADER' => 'Menu',
    'DD_SIDEBAR_CATEGORYTREE' => 'Catégories',
    'DD_SORT_ASC' => 'croissant',
    'DD_SORT_DESC' => 'décroissant',
    'DD_SUGGEST_HEADING' => 'Recommandation produit',
    'DD_USER_BILLING_LABEL_STATE' => 'Région:',
    'DD_USER_SHIPPING_ADD_DELIVERY_ADDRESS' => 'ajouter une adresse de livraison',
    'DD_USER_SHIPPING_LABEL_STATE' => 'Région:',
    'DD_USER_SHIPPING_SELECT_ADDRESS' => 'Sélectionner',
    'DD_WISHLIST_HEADING' => 'Liste de souhaits',
    'DETAILS_VPE_MESSAGE_1' => 'Cet article ne peut être commandé que dans des unités d\'emballage de',
    'DETAILS_VPE_MESSAGE_2' => '.',
    'FOOTER_NEWSLETTER_INFO' => 'Abonnez-vous à notre newsletter et bénéficiez de 5 € de réduction',
    'MANUFACTURERSLIDER_SUBHEAD' => 'Nos marques',
    'OF' => 'sur',
    'PAGE_DETAILS_CUSTOMERS_ALSO_BOUGHT_SUBHEADER' => 'Les clients ayant acheté ce produit ont également acheté les articles suivants:',
    'SEARCH_SUBMIT' => 'Rechercher',
    'SEARCH_TITLE' => 'Entrez un terme de recherche',
    'START_BARGAIN_HEADER' => 'Offres de la semaine',
    'START_BARGAIN_SUBHEADER' => 'Découvrez les dernières offres et bonnes affaires dans la boutique!',
    'START_NEWEST_HEADER' => 'Nouveau dans le shop',
    'START_NEWEST_SUBHEADER' => 'Nos dernières nouveautés dans les catégories yoga, méditation, bien-être et lifestyle.',
    'START_TOP_PRODUCTS_HEADER' => 'Nos meilleurs ventes',
    'START_TOP_PRODUCTS_SUBHEADER' => 'Les 5 produits les plus populaires de notre shop:',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_ACCESSORIES_SUBHEADER' => 'Les articles suivants conviennent bien à ce produit:',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_CROSSSELING_SUBHEADER' => 'Les clients ayant consulté cet article ont également consulté les produits suivants:',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_SIMILAR_SUBHEADER' => 'Ces articles pourraient aussi vous plaire:',
    'WIDGET_SERVICES_CONTACTS' => 'Contact',
    'SUCHBEGRIFF' => 'Ajouter un terme de recherche...',
    'PREIS' => 'Prix',
    'FREE' => 'Gratuit',
    'MORE_INFO' => 'Détails',
    'Newslettercode' => 'Envoyer le code de réduction par e-mail',
    'Newslettercodebetreff' => 'Votre code de réduction Bodynova',
    'Newslettercodebody' => 'code: NWSLTBN Valable pour une commande d\'un montant minimum de 30,00 €. Non cumulable avec d\'autres réductions',
    'MESSAGE_NEWSLETTER_CONGRATULATIONS' => 'Bienvenue',
    'STAY_INFORMED' => 'Recevez des nouveautés de l\'univers Bodynova!',
    'Retouren' => 'Retours',
    'BLOCK_PRICE' => 'Remise de quantité',
    'RetourenLabel' => 'Etiquette de retour',
    'VOUCHERMELDUNG' => 'Pour utiliser un bon d\'achat, veuillez tout d\'abord vous "Enregistrer" plus haut ou bien vous inscrire ("Connexion") si vous êtes déjà enregistré.',
    'REDEEM_COUPON' => 'Utilisez votre coupon',
    'VALID_UNTIL' => 'Date d\'expiration:',
    'TELEFONNUMMER' => ' +49-(0)221-35 66 35 0 / appels de France : 03.20.76.02.09',
    'OEFFNUNGSZEITEN' => 'Lun. au vend.: 10 à 16h',
    'DELIVERYTIME_DELIVERYTIME' => 'Délai de fabrication',

    'DD_DELETE_MY_ACCOUNT_WARNING' => 'Cette opération ne peut pas être annulée. Toutes les données personnelles seront définitivement effacées.',
    'DD_DELETE_MY_ACCOUNT' => 'Supprimer le compte',
    'DD_DELETE_MY_ACCOUNT_CONFIRMATION_QUESTION' => 'Êtes-vous sûr de vouloir supprimer votre compte ?',
    'DD_DELETE_MY_ACCOUNT_CANCEL' => 'Abandonner',
    'DD_DELETE_MY_ACCOUNT_SUCCESS' => 'Votre compte a été supprimé',
    'DD_DELETE_MY_ACCOUNT_ERROR' => 'Le compte n\'a pas pu être supprimé',

    // Account -> My product reviews
    'DD_DELETE_REVIEW_AND_RATING' => 'Supprimer l\'avis et le classement par étoiles',
    'DD_REVIEWS_NOT_AVAILABLE' => 'Il n\'y a pas d\'avis',
    'DD_DELETE_REVIEW_CONFIRMATION_QUESTION' => 'Êtes-vous sûr de vouloir effacer l\'avis ?',

    // Contact page
    'DD_SELECT_SALUTATION' => 'Veuillez sélectionner',

    'DD_CATEGORY_RESET_BUTTON' => 'Réinitialiser',
    'AVAILABLE_ON' => 'Réservez ce produit dès maintenant ! Ce produit sera à nouveau en stock à partir du %s et livrable immédiatement.',
    'ZAHLUNGSMOEGLICHKEITEN' => 'MODALITÉS DE PAIEMENT',
    'VERSANDMIT' => 'ENVOI AVEC',
    'NEWSLETTERFOOTER' => 'Restez informé',
    'ABOUT_BODYNOVA' => 'A propos de Bodynova',
    'ORDERGBINFO' => 'Le montant minimal de commande de 160 euros pour une livraison au Royaume-Unifr no\'est pas encore atteint.'

);